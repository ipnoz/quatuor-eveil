
const gulp = require('gulp');
const gutil = require('gulp-util');
const del = require('del');
const rename = require("gulp-rename");
const preservetime = require('gulp-preservetime');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const uglifyjs = require('uglify-es');
const composer = require('gulp-uglify/composer');
const jsMinifyier = composer(uglifyjs, console);

const SRC = './src';
const DEST = './dist';

const HTTP_PATH = '/var/www/html';
const START_PATH = __dirname.replace(HTTP_PATH, '')+'/'+DEST;

// Wordpress plugin
const PLUGIN_DIRNAME = './quatuoreveil.fr-wp-plugin';
const SRC_WP_PLUGIN = PLUGIN_DIRNAME;
const DEST_WP_PLUGIN = HTTP_PATH+'/wordpress/wp-content/plugins/'+PLUGIN_DIRNAME;

/*
* TASKS
*/

// Clean
gulp.task('clean', function () {
    return del([DEST]);
});

gulp.task('wp:clean', function () {
    return del([DEST_WP_PLUGIN]);
});

// vendor
gulp.task('vendor', function() {
    gulp.src([SRC+'/vendor/*/*.min.*'])
        .pipe(gulp.dest(DEST+'/vendor/'))
        .pipe(preservetime());
    gulp.src([SRC+'/vendor/fonts/*'])
        .pipe(gulp.dest(DEST+'/vendor/fonts'))
        .pipe(preservetime());
    return gulp.src([SRC+'/vendor/webfonts/*'])
        .pipe(gulp.dest(DEST+'/vendor/webfonts'))
        .pipe(preservetime());
});

// html
gulp.task('html', function () {
    return gulp.src([SRC+'/*.php'])
        .pipe(gulp.dest(DEST+'/'))
        .pipe(browserSync.stream());
});

// css
gulp.task('css', function () {
    return gulp.src([SRC+'/scss/**/*.scss'])
        .pipe(sass.sync({outputStyle:'compressed'}).on('error', sass.logError))
        .pipe(cleanCSS({inline: 'none'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(DEST+'/css'))
        .pipe(browserSync.stream());
});

gulp.task('wp:css', function () {
    return gulp.src([SRC_WP_PLUGIN+'/scss/**/*.scss'])
        .pipe(sass.sync({outputStyle:'compressed'}).on('error', sass.logError))
        .pipe(cleanCSS({inline: 'none'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(SRC_WP_PLUGIN))
        .pipe(browserSync.stream());
});

// js
gulp.task('js', function () {
    return gulp.src(SRC+'/js/**/*.js')
        .pipe(jsMinifyier().on('error', function(err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
            this.emit('end');
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(DEST+'/js/'))
        .pipe(browserSync.stream());
});

// php
gulp.task('php', function() {
    return gulp.src(SRC+'/php/**/*.php')
        .pipe(gulp.dest(DEST+'/php/'))
        .pipe(preservetime())
        .pipe(browserSync.stream());
});

gulp.task('wp:php', function() {
    return gulp.src(SRC_WP_PLUGIN+'/**/*.php')
        .pipe(gulp.dest(DEST_WP_PLUGIN))
        .pipe(preservetime());
});

// img
gulp.task('img', function () {
    return gulp.src(SRC+'/img/*')
        .pipe(gulp.dest(DEST+'/img/'))
        .pipe(preservetime());
});

function browserSyncDaemon() {
    browserSync.init({
        proxy: '172.18.0.1',
        open: false,
        notify: false,
        logFileChanges: false,
        reloadOnRestart: true,
        ghostMode: false,
        startPath: START_PATH
    });
}

function watchFiles() {
    gulp.watch(SRC+'/*.php', gulp.series('html'));
    gulp.watch(SRC+'/scss/**/*.scss', gulp.series('css'));
    gulp.watch(SRC+'/js/**/*.js', gulp.series('js'));
    gulp.watch(SRC+'/php/**/*.php', gulp.series('php'));
}

function watchWpPluginFiles() {
    gulp.watch(SRC_WP_PLUGIN+'/**/*.php', gulp.series('wp:php'));
    gulp.watch(SRC_WP_PLUGIN+'/scss/**/*.scss', gulp.series('wp:css'));
}

gulp.task('default', gulp.parallel(browserSyncDaemon, watchFiles, watchWpPluginFiles));
gulp.task('build', gulp.series('vendor', 'html', 'css', 'js', 'img', 'php'));
gulp.task('rebuild', gulp.series('clean', 'build'));

gulp.task('wp:build', function () {
    return gulp.src(SRC_WP_PLUGIN+'/**/*')
        .pipe(gulp.dest(DEST_WP_PLUGIN))
        .pipe(preservetime());
});
gulp.task('wp:rebuild', gulp.series('wp:clean', 'wp:build'));
