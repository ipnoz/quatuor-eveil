(function($) {

    "use strict";

    /**
     * Remove query string from URL
     */
    $(document).ready(function() {
        var uri = window.location.toString();
        if (uri.indexOf('?') > 0) {
            var clean_uri = uri.substring(0, uri.indexOf('?'));
            window.history.replaceState({}, document.title, clean_uri);
        }
    });

    var menuEl = '#nav #menu';
    var menuToggleTime = 250;

    /**
     * Toggle responsive menu
     *
     * Override jQuery.toggle() method action:
     * Remove style="display:[block|none]" injected into the HTML element
     * and add css class "show"
     */
    function toggleMenu() {
        $(menuEl).toggle(menuToggleTime, function() {
            $(this).css('display', '').toggleClass('show');
        });
    }

    function closeMenu() {
        $(menuEl).hide(menuToggleTime, function() {
            $(this).css('display', '').removeClass('show');
        });
    }

    /**
     * Action button: toggle responsive menu
     */
    $('#nav button').on('click', toggleMenu);

    /**
     * js-scroll-trigger event
     */
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
        /**
         * Reset scrollTop animations before run another one.
         * Avoid to queue many animations on multiple click and be stucked waiting for them to finish.
         */
        $('html, body').stop();

        /**
         * Closes responsive menu when a scroll trigger link is clicked
         */
        if (window.innerWidth < 992) {
            closeMenu();
        }

        /**
         * Smooth scrolling on all links inside the navbar
         */
        if (this.hash !== '') {
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1000, 'easeInOutExpo');
            return false;
        }
    });

    /**
     * If available, activate Bootstrap.Scrollspy to add .active class on navbar items during scroll.
     */
    if (typeof $().scrollspy == 'function') {
        $('body').scrollspy({
            target: menuEl
        });
    }

    /**
     * Activate AOS.js
     */
    if (typeof AOS == 'object') {
        AOS.init({
            // disable: 'mobile',
            // disable: true,
            // easing: 'easeInQuint',
            // offset: 300,
        });
    }

})(jQuery);
