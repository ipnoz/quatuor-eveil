<?php

namespace App\Handler;

class DatasHandler {

    private $datas = [];

    public function setDatas($datas) {
        $this->datas = $datas;
    }

    public function get($key) {
        foreach($this->datas as $data) {
            if ($data['data_key'] === $key) {
                return $data['data_value'];
            }
        }
    }
}
