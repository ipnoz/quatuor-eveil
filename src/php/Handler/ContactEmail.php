<?php

// Disabling the ContactEmail
redirectToMain('error');

define('MAIL_EOL', "\r\n");

/**
 * Redirection to main
 */
function redirectToMain($result) {
    header('Location:?sentStatus='.$result);
    die();
}

/**
 * HTML submission validator
 */
if (
    $_POST['name'] === ''
    OR $_POST['email'] === ''
    OR $_POST['message'] === ''
){
    redirectToMain('error');
}

if (! filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
    redirectToMain('error');
}

$name = strip_tags(htEnc($_POST['name']));
$email_address = strip_tags(htEnc($_POST['email']));
$subject = strip_tags(htEnc($_POST['subject']));
$message = strip_tags(htEnc($_POST['message']));

/**
 * Create the email and send the message
 */
$headers = 'From: '.$email_address.MAIL_EOL;
$email_subject = 'Message depuis '.$_SERVER['HTTP_HOST'].'/quatuor-eveil';
$email_body = 'De: '.$name.MAIL_EOL;
$email_body .= 'Sujet: '.$subject.MAIL_EOL . MAIL_EOL;
$email_body .= $message;

$result = mail(
    QE4_CONTACT_EMAIL,
    $email_subject,
    $email_body,
    $headers
);

redirectToMain($result ? 'success' : 'error');
