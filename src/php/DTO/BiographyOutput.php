<?php

namespace App\DTO;

class BiographyOutput
{
    private $encart;
    private $complement;

    public function __construct($encart, $complement)
    {
        $this->encart = $encart;
        $this->complement = $complement;
    }

    /**
     * @return mixed
     */
    public function getEncart()
    {
        return $this->encart;
    }

    /**
     * @return mixed
     */
    public function getComplement()
    {
        return $this->complement;
    }
}
