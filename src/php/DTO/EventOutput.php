<?php

namespace App\DTO;

class EventOutput
{
    private $day;
    private $month;
    private $year;
    private $time;
    private $title;
    private $venue;
    private $address;
    private $city;

    public function __construct($start, $title, $venue, $address, $city)
    {
        $this->day = date('d', $start);
        $this->month = strftime('%h', $start);
        if (date('Y', $start) > date('Y')) {
            $this->year = date('Y', $start);
        }
        $this->time = date('H:i', $start);

        $this->title = $title;
        $this->venue = $venue;
        $this->address = $address;
        $this->city = $city;
    }


    /**
     * @return false|string
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @return string
     */
    public function getMonth(): string
    {
        return $this->month;
    }

    /**
     * @return false|string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @return false|string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }
}
