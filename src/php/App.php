<?php

require_once 'init.php';

if (isset($_POST['qe4_html_action'])) {
    if ($_POST['qe4_html_action'] === 'contact_email') {
        require_once PATH_APP.'Handler/ContactEmail.php';
    }

}
require_once PATH_APP.'dbConnection.php';

use App\Repository\DatasRepository;
use App\DTO\BiographyOutput;
use App\Handler\DatasHandler;

$Datas = new DatasHandler();

/**
 * Get QE4 datas
 */
if (null !== $db) {
    $repository = new DatasRepository($db);
    $Datas->setDatas($repository->findAll());
}

/**
 * Setup biographies
 */
$audrey = new BiographyOutput(
    textarea2html($Datas->get('bio_encart_audrey')),
    textarea2html($Datas->get('bio_complement_audrey'))
);
$francois = new BiographyOutput(
    textarea2html($Datas->get('bio_encart_francois')),
    textarea2html($Datas->get('bio_complement_francois'))
);
$clement = new BiographyOutput(
    textarea2html($Datas->get('bio_encart_clement')),
    textarea2html($Datas->get('bio_complement_clement'))
);
$yoann = new BiographyOutput(
    textarea2html($Datas->get('bio_encart_yoann')),
    textarea2html($Datas->get('bio_complement_yoann'))
);
