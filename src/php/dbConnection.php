<?php

$db = null;

if (extension_loaded('pdo')) {
    try {
        $db = new PDO('mysql:host='.$dbhost.';dbname='.$dbname, $dbuser, $dbpw);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->exec('SET NAMES '.$dbcharset);
    }
    catch(PDOException $e) {
        if (true === QE_DEBUG) {
            debugError('PDOException: '.$e->getMessage());
        }
    }
}
