<?php

/**
 * Autoload namespace helper
 *
 * @param $class
 */
function autoload($class)
{
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
    $path = str_replace('App', '', $path);
    require_once(PATH_APP.$path.'.php');
}

spl_autoload_register('autoload');

/**
 * Print debug HTML error message helper
 *
 * @param $message - string
 */
function debugError($message)
{
    echo '<div class="debugError">Debug error: '.htEnc($message).'<div>';
}

/**
 * htmlEntities helper
 *
 * @param $str - string to convert
 * @return string
 */
function htEnc($str)
{
    return htmlEntities($str, ENT_QUOTES, 'UTF-8');
}

/**
 * Replace textarea tag ([i], [strong], etc) to the equivalent in HTML (<i>, <strong>)
 */
function tag2html($subject)
{
    $search = ['[strong]', '[/strong]', '[i]', '[/i]'];
    $replace = ['<strong>', '</strong>', '<i>', '</i>'];
    return str_replace($search, $replace, $subject);
}

/**
 * Mutliple action to performe on one line input datas from DB to HTML
 */
function input2html($message)
{
    return tag2html(htEnc($message));
}

/**
 * Mutliple action to performe on textarea datas from DB to HTML
 */
function textarea2html($message)
{
    return tag2html(nl2br(htEnc($message)));
}
