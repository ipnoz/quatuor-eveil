<?php

namespace App\Repository;

use \PDO;
use \PDOException;
use \PDOStatement;

class EventRepository extends Repository
{
    /**
     * Concert duration before considering it as passed
     */
    private $concertDuration = 8*60*60;

    /**
     * @return array
     */
    public function findAll($limit = null)
    {
        $sql =
            'SELECT
                id,
                start,
                venue,
                address,
                city,
                post_id,
                post_status,
                post_title
            FROM
                '.$this->tablePrefix.'ai1ec_events t1
            INNER JOIN
                '.$this->tablePrefix.'posts t2
            ON
                t1.post_id = t2.id
            AND
                post_status = \'publish\'
            WHERE
                start + '.$this->concertDuration.' > '.time().'
            ORDER BY
                start';

        if (is_int($limit)) {
            $sql .= ' LIMIT '.$limit;
        }

        try {
            $results = $this->db->query($sql);
        }
        catch(PDOException $e) {
            $results = null;
        }

        if ($results instanceof PDOStatement) {
            $this->results = $results->fetchAll(PDO::FETCH_ASSOC);
        }

        return $this->results;
    }
}
