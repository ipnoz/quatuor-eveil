<?php

namespace App\Repository;

use \PDO;
use \PDOException;
use \PDOStatement;

class DatasRepository extends Repository
{
    /**
     * @return array
     */
    public function findAll()
    {
        $sql =
            'SELECT
                qe4_key AS data_key,
                qe4_value AS data_value
            FROM
                '.$this->tablePrefix.'QE4';

        try {
            $results = $this->db->query($sql);
        }
        catch(PDOException $e) {
            $results = null;
        }

        if ($results instanceof PDOStatement) {
            $this->results = $results->fetchAll(PDO::FETCH_ASSOC);
        }

        return $this->results;
    }
}
