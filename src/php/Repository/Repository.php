<?php

namespace App\Repository;

use \PDO;

class Repository
{
    /**
     * @var array - results to return after a query
     */
    protected $results = [];

    /**
     * @var PDO - database object
     */
    protected $db;

    protected $tablePrefix = 'wp_';

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }
}
