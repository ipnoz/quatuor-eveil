<?php

if (!isset($_GET['sentStatus'])) {
    return;
}

$status = $_GET['sentStatus'];

$successText = 'Le message a été envoyé aux musiciens avec succès';
$errorText = 'Une erreur est survenue, le message n\'a pas été envoyé';

$text = ($status === 'success') ? $successText : $errorText;

?>
        <div class="alert <?= $status; ?>" role="alert">
            <?= $text, PHP_EOL; ?>
        </div>

