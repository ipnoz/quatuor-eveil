<?php

use App\Repository\EventRepository;
use App\DTO\EventOutput;

/**
 * A valid db connection is required
 */
if (null === $db) {
    return;
}

/**
 * Find all events
 */
$repository = new EventRepository($db);
$results = $repository->findAll();

/**
 * Transform results for output
 */
$datas = [];
foreach ($results as $event) {
    $eventOutput = new EventOutput(
        $event['start'],
        $event['post_title'],
        $event['venue'],
        $event['address'],
        $event['city']
    );
    $datas [] = $eventOutput;
}

/**
 * Print datas
 */
foreach ($datas as $d): ?>
                <div class="event">
                    <div class="date-area">
                        <h2><?= $d->getDay(); ?></h2>
                        <h4><?= $d->getMonth(); ?></h4>
                        <h3><?= $d->getYear(); ?></h3>
                    </div>
                    <h4 class="time-area"><?= $d->getTime(); ?></h4>
                    <div class="description-area">
                        <h3><?= $d->getTitle(); ?></h3>
                        <h4><?= $d->getVenue(); ?></h4>
                        <h5><?= $d->getAddress(); ?></h5>
                        <h5 class="city"><?= $d->getCity(); ?></h5>
                    </div>
                </div>
<?php endforeach;
