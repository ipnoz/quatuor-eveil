<?php

/**
 * If user has disactived annouce, return.
 */
if (!$Datas->get('annonce_isActive')) {
    return;
}

$title = input2html($Datas->get('annonce_title'));
$encartTop = input2html($Datas->get('annonce_encart_top'));
$encartBottom = input2html($Datas->get('annonce_encart_bottom'));
$message = textarea2html($Datas->get('annonce_message'));
$url1 = $Datas->get('annonce_url1');
$url2 = $Datas->get('annonce_url2');

$displayEncart = ($encartTop !== '' OR $encartBottom !== '');

?>
            <section id="announce">
                <div class="top">
<?php if ($title !== ''): ?>
                    <h3 class="title"><?= $title; ?></h3>
<?php endif; ?>
<?php if ($displayEncart): ?>
                    <div class="encart">
                        <h2><?= $encartTop; ?></h2>
                        <h3><?= $encartBottom; ?></h3>
                    </div>
<?php endif; ?>
                </div>
<?php if ($message !== ''): ?>
                <div class="message">
                    <p>
                        <?= $message; ?>
                        <i class="fas fa-music"></i>
                        <i class="fas fa-guitar audrey"></i>
                        <i class="fas fa-guitar francois"></i>
                        <i class="fas fa-guitar clement"></i>
                        <i class="fas fa-guitar yoann"></i>
                    </p>
                </div>
<?php endif; ?>
<?php if (!empty($url1)): ?>
                <div class="media">
                    <img src="<?= $url1; ?>" alt="announce-image.jpg"/>
                </div>
<?php endif; ?>
<?php if (!empty($url2)): ?>
                <div class="youtube-video" data-aos="zoom-in">
                    <iframe src="<?= $url2; ?>?rel=0&controls=1&showinfo=0&modestbranding=1" allowfullscreen allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture">
                    </iframe>
                </div>
<?php endif; ?>
            </section>
