<?php require_once 'php/App.php'; ?>
<!DOCTYPE html>
<html lang="fr" class="no-js" id="top">

    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

        <title>
            Quatuor Eveil
        </title>

        <meta name="description" content="Un groupe de musique qui réveille"/>
        <meta name="author" content="David Dadon - https://www.ipnoz.net"/>
        <meta name="keywords"
              content="musique, bordeaux"/>
        <meta name="copyright" content="Quatuor Eveil"/>
        <meta name="Language" content="fr"/>

        <meta name="apple-mobile-web-app-title" content="Quatuor Eveil"/>
        <meta name="application-name" content="Quatuor Eveil"/>
        <meta name="msapplication-TileColor" content="#da532c"/>
        <meta name="theme-color" content="#ffffff"/>

        <meta property="og:title" content="Quatuor Eveil"/>
        <meta property="og:site_name" content="Quatuor Eveil"/>
        <meta property="og:description" content="Un groupe de musique qui réveille"/>
        <meta property="og:url" content="https://www.quatuoreveil.fr/"/>
        <meta property="og:type" content="article"/>
        <meta property="og:image" content="https://www.quatuoreveil.fr/img/header-lg.jpg"/>
        <meta property="og:locale" content="fr"/>
        <meta name="twitter:card" content="summary_large_image"/>

        <link rel="apple-touch-icon" sizes="96x96" href="img/logo-96x85.png"/>
        <link rel="icon" type="image/png" sizes="32x32" href="img/logo-32x28.png"/>
        <link rel="icon" type="image/png" sizes="16x16" href="img/logo-16x14.png"/>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:400,800"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700"/>

        <link rel="stylesheet" href="vendor/css/fontAwesome-all.min.css"/>
        <link rel="stylesheet" href="vendor/css/aos.min.css"/>
        <link rel="stylesheet" href="css/styles.min.css"/>

        <script src="vendor/js/HTML5-boilerplate.min.js"></script>
        <script src="vendor/js/jquery.min.js"></script>
        <script>
            $('html').removeClass('no-js').addClass('js');
        </script>
    </head>

    <body>

        <div id="preloader">
            <div class="bg">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="animation">
                <span>Q</span>
                <span>U</span>
                <span>A</span>
                <span>T</span>
                <span>U</span>
                <span>O</span>
                <span>R</span>
                <br/>
                <span>É</span>
                <span>V</span>
                <span>E</span>
                <span>I</span>
                <span>L</span>
            </div>
        </div>
        <script src="js/preloader.min.js"></script>

        <nav id="nav">
            <a class="js-scroll-trigger" href="#top">
                <img class="logo" src="img/logo-sm.jpg" alt="logo.jpg"/>
            </a>
            <button type="button" aria-controls="menu" aria-expanded="false" aria-label="Toggle menu">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div id="menu">
                <div class="container">
                    <ul>
                        <li>
                            <a class="nav-link js-scroll-trigger audrey" href="#concerts">concerts</a>
                        </li>
                        <li>
                            <a class="nav-link js-scroll-trigger francois" href="#album">album</a>
                        </li>
                        <li>
                            <a class="nav-link js-scroll-trigger clement" href="#videos">vidéos</a>
                        </li>
                        <li>
                            <a class="nav-link js-scroll-trigger yoann" href="#contact">contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

<?php require_once PATH_APP.'views/email_status.php'; ?>

        <div id="main">

            <header></header>

<?php require_once PATH_APP.'views/announce.php'; ?>

            <section id="presentation">
                <h4>
                    Quatuor Eveil
                </h4>
                <hr>
                <p data-aos="zoom-in">
                    Redécouvrez la guitare classique grâce à des compositions originales où se mêlent des influences aux
                    sonorités classiques, sud-américaines et modernes.
                    Le Quatuor Eveil, composé de guitaristes professionnels passionnés, vous propose un véritable voyage
                    musical dont le programme est entièrement écrit par les musiciens.
                </p>
            </section>

            <section id="musiciens">
                <div class="musicien audrey">
                    <img src="img/audrey.jpg" alt="audrey.jpg" data-aos="fade"/>
                    <div class="bio light">
                        <h4>Audrey Paquet</h4>
                        <p class="encart" data-aos="zoom-in">
                            <?= $audrey->getEncart(), PHP_EOL; ?>
                        </p>
                        <p class="complement" data-aos="zoom-in">
                            <?= $audrey->getComplement(), PHP_EOL; ?>
                        </p>
                    </div>
                </div>
                <div class="musicien francois">
                    <img src="img/francois.jpg" alt="francois.jpg" data-aos="fade"/>
                    <div class="bio dark">
                        <h4>François Chappey</h4>
                        <p class="encart" data-aos="zoom-in">
                            <?= $francois->getEncart(), PHP_EOL; ?>
                        </p>
                        <p class="complement" data-aos="zoom-in">
                            <?= $francois->getComplement(), PHP_EOL; ?>
                        </p>
                    </div>
                </div>
                <div class="musicien clement">
                    <img src="img/clement.jpg" alt="clement.jpg" data-aos="fade"/>
                    <div class="bio light">
                        <h4>Clément Chappey</h4>
                        <p class="encart" data-aos="zoom-in">
                            <?= $clement->getEncart(), PHP_EOL; ?>
                        </p>
                        <p class="complement" data-aos="zoom-in">
                            <?= $clement->getComplement(), PHP_EOL; ?>
                        </p>
                    </div>
                </div>
                <div class="musicien yoann">
                    <img src="img/yohan.jpg" alt="yohan.jpg" data-aos="fade"/>
                    <div class="bio dark">
                        <h4>Yoann Penchaud</h4>
                        <p class="encart" data-aos="zoom-in">
                            <?= $yoann->getEncart(), PHP_EOL; ?>
                        </p>
                        <p class="complement" data-aos="zoom-in">
                            <?= $yoann->getComplement(), PHP_EOL; ?>
                        </p>
                    </div>
                </div>
            </section>

            <section id="concerts">
<?php require_once PATH_APP.'views/concerts.php'; ?>
            </section>

            <section id="album">
                <div>
                    <p class="one">
                        Pour leur premier album, <strong class="musicien francois">François <span>Chappey</span></strong>,
                        <strong class="musicien yoann">Yoann <span>Penchaud</span></strong>,
                        <strong class="musicien audrey">Audrey <span>Paquet</span></strong> et
                        <strong class="musicien clement">Clément <span>Chappey</span></strong> interprètent une série
                        de pièces originales et personnelles de leur composition qui reflètent leurs influences aux
                        sonorités classiques, sud-américaines et modernes.
                    </p>
                    <p class="two">
                        Le <strong>Quatuor EVEIL</strong> vous propose de redécouvrir la guitare classique au travers
                        d'un répertoire inédit, symbole des personnalités de chaque musicien.
                    </p>
                </div>
                <img src="img/album-cover-front.jpg" alt="album-cover-front.jpg"/>
                <img src="img/album-cover-back.jpg" alt="album-cover-back.jpg"/>
            </section>

            <section id="videos">
            <!--
                <h5>Clip</h5>
                <h4>Souvenir de Florence</h4>
                <div class="youtube-video" data-aos="zoom-in">
                    <iframe src="https://www.youtube-nocookie.com/embed/5KvELIJQqHs?rel=0&controls=1&showinfo=0&modestbranding=1"
                            allowfullscreen allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture">
                    </iframe>
                </div>
            -->
            </section>

            <section id="contact">
                <div class="container">

                    <form method="post">
                        <input type="hidden" name="qe4_html_action" value="contact_email"/>
                        <div class="box1">
                            <h4>
                                Nous contacter
                            </h4>
                            <div class="input">
                                <input type="text" name="name" placeholder="Votre nom" required="required"/>
                            </div>
                            <div class="input">
                                <input type="email" name="email" placeholder="Votre email" required="required"/>
                            </div>
                            <div class="input">
                                <input type="text" name="subject" placeholder="Le sujet (facultatif)"/>
                            </div>
                            <div class="submit">
                                <button type="submit">
                                    Envoyer
                                </button>
                            </div>
                        </div>
                        <div class="box2">
                            <div class="input">
                                <textarea name="message" cols="30" rows="10" placeholder="Votre message" required="required" maxlength="999"></textarea>
                            </div>
                        </div>
                    </form>
                </div>

            </section>

            <section id="social">
                <a href="https://soundcloud.com/user-80063914/sets/reflet/s-1LPUI" title="Souncloud" data-aos="zoom-in" data-aos-delay="350" data-aos-once="true">
                    <i class="fab fa-soundcloud" aria-hidden="true"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCb_Jfo_t3IYk6dXveufaV-A" title="Youtube" data-aos="zoom-in" data-aos-delay="700" data-aos-once="true">
                    <i class="fab fa-youtube" aria-hidden="true"></i>
                </a>
                <a href="https://www.facebook.com/quatuoreveil/" title="Facebook" data-aos="zoom-in" data-aos-delay="1000" data-aos-once="true">
                    <i class="fab fa-facebook-square" aria-hidden="true"></i>
                </a>
                <a href="https://www.instagram.com/quatuoreveil/" title="Instagram" data-aos="zoom-in" data-aos-delay="1350" data-aos-once="true">
                    <i class="fab fa-instagram" aria-hidden="true"></i>
                </a>
                <a href="mailto:quatuoreveil@gmail.com" title="Mailto" data-aos="zoom-in" data-aos-delay="1700" data-aos-once="true">
                    <i class="fas fa-at" aria-hidden="true"></i>
                </a>
            </section>

            <footer>
                <div class="copyright">
                    &copy; <script>document.write(new Date().getFullYear());</script> Quatuor Eveil | All rights reserved
                </div>
                <div class="copyright">
                    Site web élaboré par <a href="https://www.ipnoz.net">David Dadon</a>
                </div>
            </footer>

        </div>

        <script src="vendor/js/bootstrap.min.js"></script>
        <script src="vendor/js/jquery.easing.min.js"></script>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script src="js/app.min.js"></script>
    </body>
</html>
