
QuatuorEveil.fr
=======

### Prerequisites

* PHP >= 7.0.8
* MySQL Server  >= 5.6
* Wordpress >= 5.0
* Composer
* Node.js ^11
* npm
* Gulp

### Environment setups

###### Dev
1. Checkout code from git
    * `git clone quatuoreveil.fr`
2. Retrieve composer dev dependencies (for codeception)
    * `composer install`
3. Retrieve node.js dev dependencies
    * `npm install`
5. Compile web site datas from /src/ to create /dist/
    * `gulp build`
6. Copy wordpress admin plugin to the wordpress directory
    * `gulp wp:copy`
7. Run tests, and populate web site datas
    * `php ./vendor/bin/codeception run`
    
### Useful commands
    
###### Watch sources files with Gulp, reload web page on change
* `gulp`

###### Deploy /dist/ site to the prod
* `./deploy.sh`