<?php

use Page\Wordpress\LoginPage as WpLoginPage;
use Page\Wordpress\MusiciensPage as WpPage;
use Page\Web\MusiciensPage as WebPage;
use Datas\MusiciensDatas as Datas;
use Db\MusiciensDb as Db;

/**
 * Class MusiciensCest
 *
 * Test wordpress plugin works for the musiciens biography and test it's also
 * works for the web page.
 */
class MusiciensCest
{
    /**
     * Login in wordpress as admin
     */
    public function _before(AcceptanceTester $I)
    {
        $loginPage = new WpLoginPage($I);
        $loginPage->login(getenv('QE4_WP_ADMIN'), getenv('QE4_WP_ADMIN_PW'));
    }

    /**
     * Scenario:
     * Check that the wp plugin works and then the datas are displaying
     * without errors in the database and the web page.
     *
     * @dataProvider musiciens_biography_Provider
     */
    public function musiciens_section_works
    (
        AcceptanceTester $I,
        \Codeception\Example $musicien
    ) {
        $name = $musicien['name'];
        $I->wantTo('Test '.$name);

        /**
         * Reset musiciens datas with the wp plugin form page.
         */
        $I->amOnPage(WpPage::getMusicienURI($name));
        $I->fillField(WpPage::encartField, '');
        $I->fillField(WpPage::complementField, '');
        $I->click(WpPage::submitButton);
        /**
         * Check it's works in the database with empty values.
         */
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_encart.$name, Db::VALUE => '']);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_complement.$name, Db::VALUE => '']);
        /**
         * Check that the input fields of the wp plugin page are empty too.
         */
        $I->seeInCurrentUrl(WpPage::PATH.$name);
        $I->seeInField(WpPage::encartField, '');
        $I->seeInField(WpPage::complementField, '');
        /**
         * Check it's works in the web page with empty HTML elements.
         */
        $I->amOnPage(WebPage::URI);
        $grab1 = $I->grabTextFrom(WebPage::getEncartEl($name));
        $grab2 = $I->grabTextFrom(WebPage::getComplementEl($name));
        $I->see(trim($grab1) === '');
        $I->see(trim($grab2) === '');

        /**
         * Inject musiciens biography datas.
         */
        $I->amOnPage(WpPage::getMusicienURI($name));
        $I->fillField(WpPage::encartField, $musicien['encart']);
        $I->fillField(WpPage::complementField, $musicien['complement']);
        $I->click(WpPage::submitButton);
        /**
         * Check it's works in the database with values equal to injected datas.
         */
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_encart.$name, Db::VALUE => $musicien['encart']]);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_complement.$name, Db::VALUE => $musicien['complement']]);
        /**
         * Check that the input fields of the wp plugin page are populated with
         * musiciens datas.
         */
        $I->seeInCurrentUrl(WpPage::PATH.$name);
        $I->seeInField(WpPage::encartField, $musicien['encart']);
        $I->seeInField(WpPage::complementField, $musicien['complement']);
        /**
         * Check it's works in the web page with non empty HTML elements.
         */
        $I->amOnPage(WebPage::URI);
        $grab1 = $I->grabTextFrom(WebPage::getEncartEl($name));
        $grab2 = $I->grabTextFrom(WebPage::getComplementEl($name));
        $I->see(trim($grab1) !== '');
        $I->see(trim($grab2) !== '');
    }

    /**
     * Musiciens datas provider function.
     *
     * @return array
     */
    private function musiciens_biography_Provider()
    {
        return [Datas::AUDREY, Datas::FRANCOIS, Datas::YOANN, Datas::CLEMENT];
    }
}
