<?php

/**
 * Class AppCest
 *
 * Check that App.php didn't crash badly.
 * But it didn't means that's App.php didn't produced some errors during the
 * process.
 */
class AppCest
{
    /**
     * The web page is complet if we can see the last footer element.
     */
    public function front_page_works(AcceptanceTester $I)
    {
        $I->wantTo('Check HTML page does not crash');
        $I->amOnPage('/');
        $I->see('Site web élaboré par David Dadon','//body//footer');
    }
}
