<?php

/**
 * Class emailCest
 *
 * Testing some scenarios for the contact email form.
 * First test will try to send a valid email.
 * Then test the handler for incorrect datas submition.
 *
 * Run "bin/codecept run -x skip" to skip the skip group ^_^
 * @group skip
 * @group email
 */
class emailCest
{
    /**
     * @dataProvider sendEmailProvider
     */
    public function sendEmailTest(AcceptanceTester $I, \Codeception\Example $example)
    {
        $I->amOnPage('/');
        $I->fillField('name', $example['name']);
        $I->fillField('email', $example['email']);
        $I->fillField('subject', $example['subject']);
        $I->fillField('message', $example['message']);
        $I->click('button');
        $I->see($example['expect']);
    }

    /**
     *
     */
    private function sendEmailProvider()
    {
        $name = 'testName';
        $email = 'test@test.tld';
        $subject = 'testSubject';
        $message = 'Test message: hello tester !';
        $succesText = 'succès';
        $errorText = 'Une erreur est survenue';
        return [
            ['name' => $name, 'email' => $email, 'subject' => $subject, 'message' => $message, 'expect' => $succesText],
            ['name' => '', 'email' => $email, 'subject' => $subject, 'message' => $message, 'expect' => $errorText],
            ['name' => $name, 'email' => '', 'subject' => $subject, 'message' => $message, 'expect' => $errorText],
            ['name' => $name, 'email' => $email, 'subject' => $subject, 'message' => '', 'expect' => $errorText],
        ];
    }
}
