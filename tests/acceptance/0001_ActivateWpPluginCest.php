<?php

use Page\Wordpress\LoginPage as WpLoginPage;
use Page\Wordpress\MusiciensPage as WpPage;
use Db\TableDb;

/**
 * Class WpPluginCest
 *
 * Tests for the worpress plugin
 */
class ActivateWpPluginCest
{
    /**
     * Login in wordpress as admin
     */
    public function _before(AcceptanceTester $I)
    {
        $loginPage = new WpLoginPage($I);
        $loginPage->login(getenv('QE4_WP_ADMIN'), getenv('QE4_WP_ADMIN_PW'));
    }

    /**
     * Test that the plugin activation create the table
     */
    public function plugin_activation_create_table(AcceptanceTester $I)
    {
        $I->amOnPage(WpPage::plugins);

        $I->click('//a[contains(@href,"action=activate&plugin=quatuoreveil.fr-wp-plugin")]');
        $I->seeElement('//a[contains(@href,"action=deactivate&plugin=quatuoreveil.fr-wp-plugin")]');
        $I->seeNumRecords(0, TableDb::TABLE);
    }
}
