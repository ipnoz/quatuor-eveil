<?php

use Page\Wordpress\LoginPage as WpLoginPage;
use Page\Wordpress\AnnouncePage as WpPage;
use Page\Web\AnnouncePage as WebPage;
use Datas\AnnounceDatas as Datas;
use Db\AnnounceDb as Db;

/**
 * Class AnnounceCest
 *
 * Test the announce section integration between the wordpress plugin and the
 * web page.
 */
class AnnounceCest
{
    /**
     * Login in wordpress as admin
     */
    public function _before(AcceptanceTester $I)
    {
        $loginPage = new WpLoginPage($I);
        $loginPage->login(getenv('QE4_WP_ADMIN'), getenv('QE4_WP_ADMIN_PW'));
    }

    /**
     * Scenario:
     * Check that the wp plugin works and then the datas are displaying
     * without errors in the database and the web page.
     */
    public function announce_section_works(AcceptanceTester $I)
    {
        /**
         * Reset announce datas with the wp plugin form page.
         */
        $I->amOnPage(WpPage::URI);
        $I->uncheckOption(WpPage::activateField);
        $I->fillField(WpPage::titleField, '');
        $I->fillField(WpPage::encartTopField, '');
        $I->fillField(WpPage::encartBottomField, '');
        $I->fillField(WpPage::messageField, '');
        $I->fillField(WpPage::url1Field, '');
        $I->fillField(WpPage::url2Field, '');
        $I->click(WpPage::submitButton);
        /**
         * Check it's works in the database with empty values.
         */
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_isActive, Db::VALUE => '']);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_title, Db::VALUE => '']);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_encartTop, Db::VALUE => '']);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_encartBottom, Db::VALUE => '']);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_message, Db::VALUE => '']);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_url1, Db::VALUE => '']);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_url2, Db::VALUE => '']);
        /**
         * Check that the input fields of the wp plugin page are empty too.
         */
//        $I->amOnPage(WpPage::URI);
        $I->seeInCurrentUrl(WpPage::PATH);
        $I->dontSeeCheckboxIsChecked(WpPage::activateField);
        $I->seeInField(WpPage::titleField, '');
        $I->seeInField(WpPage::encartTopField, '');
        $I->seeInField(WpPage::encartBottomField, '');
        $I->seeInField(WpPage::messageField, '');
        $I->seeInField(WpPage::url1Field, '');
        $I->seeInField(WpPage::url2Field, '');
        /**
         * Check that the announce section is missing in the web page
         */
        $I->amOnPage(WebPage::URI);
        $I->dontSeeElement(WebPage::announceEl);
        /**
         * Activate the announce and check that all elements are empty
         */
        $I->amOnPage(WpPage::URI);
        $I->checkOption(WpPage::activateField);
        $I->click(WpPage::submitButton);

        $I->amOnPage(WebPage::URI);
        $I->SeeElement(WebPage::announceEl);

        $I->dontSeeElement(WebPage::titleEl);
        $I->dontSeeElement(WebPage::encartTopEl);
        $I->dontSeeElement(WebPage::encartBottomEl);
        $I->dontSeeElement(WebPage::messageEl);
        $I->dontSeeElement(WebPage::url1El);
        $I->dontSeeElement(WebPage::url2El);

        /**
         * Repopulate with datas the announce section
         */
        $I->amOnPage(WpPage::URI);
        $I->fillField(WpPage::titleField, Datas::title);
        $I->fillField(WpPage::encartTopField, Datas::encartTop);
        $I->fillField(WpPage::encartBottomField, Datas::encartBottom);
        $I->fillField(WpPage::messageField, Datas::message);
        $I->fillField(WpPage::url1Field, Datas::url1);
        $I->fillField(WpPage::url2Field, Datas::url2);
        $I->click(WpPage::submitButton);
        /**
         * Check that it's works in the database with values equal to injected datas.
         */
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_isActive, Db::VALUE => '1']);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_title, Db::VALUE => Datas::title]);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_encartTop, Db::VALUE => Datas::encartTop]);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_encartBottom, Db::VALUE => Datas::encartBottom]);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_message, Db::VALUE => Datas::message]);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_url1, Db::VALUE => Datas::url1]);
        $I->seeInDatabase(Db::TABLE, [Db::KEY => Db::KEY_url2, Db::VALUE => Datas::url2]);
        /**
         * Check that the input fields of the wp plugin page are repopulated too.
         */
        $I->seeInCurrentUrl(WpPage::PATH);
        $I->seeCheckboxIsChecked(WpPage::activateField);
        $I->seeInField(WpPage::titleField, Datas::title);
        $I->seeInField(WpPage::encartTopField, Datas::encartTop);
        $I->seeInField(WpPage::encartBottomField, Datas::encartBottom);
        $I->seeInField(WpPage::messageField, Datas::message);
        $I->seeInField(WpPage::url1Field, Datas::url1);
        $I->seeInField(WpPage::url2Field, Datas::url2);
        /**
         * Check it's works in the web page with non empty HTML elements.
         */
        $I->amOnPage(WebPage::URI);
        $I->see(Datas::title, WebPage::titleEl);
        $I->see(Datas::encartTop, WebPage::encartTopEl);
        $I->see(Datas::encartBottom, WebPage::encartBottomEl);
        $I->see(Datas::messagePart, WebPage::messageEl);
        $I->seeElement(WebPage::url1El);
        $I->seeElement(WebPage::url2El);
    }
}
