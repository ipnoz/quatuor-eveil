<?php

namespace Db;

/**
 * Wordpress plugin key name for musiciens biography
 */
class MusiciensDb extends TableDb
{
    const KEY_encart = 'bio_encart_';
    const KEY_complement = 'bio_complement_';
}
