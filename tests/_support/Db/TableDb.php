<?php

namespace Db;

/**
 * Class TableDb
 *
 * Common wordpress table parameters for the plugin
 */
class TableDb
{
    /**
     * Wordpress global table prefix
     */
    const PREFIX = 'wp_';

    /**
     * Wordpress plugin table name
     */
    const TABLE = self::PREFIX.'QE4';

    /**
     * Wordpress plugin table column key name
     */
    const KEY = 'qe4_key';

    /**
     * Wordpress plugin table column value name
     */
    const VALUE = 'qe4_value';
}
