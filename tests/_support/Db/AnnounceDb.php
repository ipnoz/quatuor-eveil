<?php

namespace Db;

/**
 * Wordpress plugin key name for the announce section
 */
class AnnounceDb extends TableDb
{
    const KEY_isActive = 'annonce_isActive';
    const KEY_title = 'annonce_title';
    const KEY_encartTop = 'annonce_encart_top';
    const KEY_encartBottom = 'annonce_encart_bottom';
    const KEY_message = 'annonce_message';
    const KEY_url1 = 'annonce_url1';
    const KEY_url2 = 'annonce_url2';
}
