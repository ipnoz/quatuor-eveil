<?php

namespace Datas;

/**
 * Class AnnounceDatas
 *
 * Datas for the announce section
 */
class AnnounceDatas
{
    const title = 'en concert';
    const encartTop = '3 mai 2019';
    const encartBottom = 'la grande poste';
    const message = 'Pour la sortie de notre nouvel album, [strong]Reflet[/strong], nous avons le plaisir de vous annoncer que nous sommes en concert le vendredi 03 mai 2019 à la Grande Poste.
Venez nombreux';
    const messagePart = 'Pour la sortie de notre nouvel album,';
    const url1 = 'https://www.quatuoreveil.fr/img/album-affiche-bus.jpg';
    const url2 = 'https://www.youtube-nocookie.com/embed/WQ1oUKgSx9E';
}
