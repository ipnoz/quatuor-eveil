<?php

namespace Datas;

/**
 * Class MusiciensDatas
 *
 * Datas for the musiciens biography
 */
class MusiciensDatas
{
    const AUDREY = [
        'name' => 'audrey',
        'encart' => 'Commence l\'étude de la guitare dès l\'âge de neuf ans avec [strong]Marie-Pierre Burving[/strong] et complète sa formation avec [strong]Fernando Millet[/strong]. Elle travaille avec [strong]Raul Maldonado[/strong], [strong]Roberto Aussel[/strong] au cours de nombreux stages et entre au Conservatoire de Bordeaux dans la classe d\'[strong]Olivier Chassain[/strong] où elle obtient le premier prix de guitare.',
        'complement' => 'Ses rencontres l\'amènent à jouer dans diverses formations aux influences variées: du tango avec le « [strong][i]Trio Milonga[/i][/strong] » ou encore du métal acoustique dans Apatrides. Elle enseigne également dans des écoles de musiques depuis 2005.'
    ];
    const FRANCOIS = [
        'name' => 'francois',
        'encart' => '[strong]Guitariste classique[/strong].
Il a étudié au CRR de Bordeaux (Conservatoire à rayonnement régional) depuis 1992 dans la classe de [strong]Laurence Postigo[/strong] puis d’[strong]Olivier Chassain[/strong]. Il obtient son D.E.M (diplôme d’étude musicales) à Bordeaux avec mention très bien. Il étudie à Paris dans la classe de Tania Chaniot puis avec [strong]Jean Luc Rochietti[/strong] à Bordeaux. En 2008 il acquiert son diplôme d’état de professeur de guitare classique ainsi qu’une licence de musicologie avec mention. Il décroche le premier prix du concours artistique d’Epinal en 2009. Il enseigne au conservatoire de Saintes depuis 2009 et se produit en concert avec divers formations.',
        'complement' => 'Il forme en 2011 le duo « [strong][i]Gardenias[/i][/strong] » avec la chanteuse [strong]Carolina Carmona[/strong] : une rencontre entre le chant latino-américain et la guitare classique. Depuis 2013 il se forme aux musiques improvisées dans l\'école "[strong]Music Woksshops[/strong]" avec [strong]Rija Randrianivosoa[/strong] (jazz, blues, latin-jazz, afro-jazz).'
    ];
    const YOANN = [
        'name' => 'yoann',
        'encart' => 'S’intéresse à la guitare classique à l’âge de 16 ans. Autodidacte la première année, il commencera ses études à 17 ans auprès de [strong]Caroline Bacou[/strong] avant d’intégrer le conservatoire de Bordeaux et d’y obtenir son Diplôme d’Etudes Musicales (DEM) dans la classe d’[strong]Olivier Chassain[/strong]. Au cours de sa formation, il travailla également avec [strong]Eric Francerie[/strong], [strong]Jean-Pierre Charbonnel[/strong], [strong]Rodolfo Lahoz[/strong] et [strong]Stein-Erik Olsen[/strong] lors de stages.',
        'complement' => 'Attiré depuis toujours par la composition, il décide de constituer le [strong]Quatuor[/strong] « [strong]Eveil[/strong] » dont l’objectif est de jouer un répertoire formé de pièces créées par les musiciens de la formation. Parallèlement, Yoann enseigne dans des écoles de musique depuis 2004.'
    ];
    const CLEMENT = [
        'name' => 'clement',
        'encart' => 'Commence la guitare classique à l\'âge de six ans au CRR de Bordeaux. Il est élève de [strong]Laurence Postigo[/strong], puis d\'[strong]Olivier Chassain[/strong]. Après avoir obtenu son Diplôme d’Études Musicales mention très bien en guitare et en musique de chambre, il poursuit son apprentissage de la musique au CEFEDEM Bordeaux Aquitaine, où il obtient son Diplôme d’État de professeur de musique spécialité guitare classique, ainsi qu\'une licence de musicologie. Depuis 2010, il enseigne au conservatoire de Libourne.',
        'complement' => 'Non empty'
    ];
}
