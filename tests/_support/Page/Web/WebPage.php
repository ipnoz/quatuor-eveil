<?php

namespace Page\Web;

/**
 * Class WebPage
 *
 * Common elements for the web page
 */
class WebPage
{
    /**
     * URI of the web page
     */
    const URI = '/';
}
