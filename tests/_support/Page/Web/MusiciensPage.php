<?php

namespace Page\Web;

/**
 * Class MusiciensPage
 *
 * Enum HTML locator for the musiciens section in the web page.
 */
class MusiciensPage extends WebPage
{
    /**
     * The musiciens section HTML element locator
     */
    const musiciensEl = 'section#musiciens';

    /**
     * Return the single musicien encart locator
     */
    static public function getEncartEl($name)
    {
        return self::musiciensEl . sprintf(' .musicien.%s .encart', $name);
    }

    /**
     * Return the single musicien complement locator
     */
    static public function getComplementEl($name)
    {
        return self::musiciensEl . sprintf(' .musicien.%s .complement', $name);
    }
}
