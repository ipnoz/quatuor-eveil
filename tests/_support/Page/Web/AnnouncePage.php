<?php

namespace Page\Web;

/**
 * Class AnnouncePage
 *
 * Enum HTML locator for the announce section in the web page.
 */
class AnnouncePage extends WebPage
{
    /**
     * The announce section HTML element locator
     */
    const announceEl = 'section#announce';

    /**
     * Announce HTML elements locator
     */
    const titleEl = self::announceEl.' h3.title';
    const encartTopEl = self::announceEl.' .encart h2';
    const encartBottomEl = self::announceEl.' .encart h3';
    const messageEl = self::announceEl.' .message';
    const url1El = self::announceEl.' img';
    const url2El = self::announceEl.' .youtube-video iframe';
}
