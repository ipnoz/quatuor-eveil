<?php

namespace Page\Wordpress;

/**
 * Class LoginPage
 *
 * Helper class for login in wordpress
 */
class LoginPage extends WordpressPage
{
    /**
     * Wordpress login URI
     */
    const URI = self::URL.'wp-login.php';

    /**
     * Wordpress login elements locator
     */
    const usernameField = '#user_login';
    const passwordField = '#user_pass';
    const rememberMeField = 'rememberme';
    const loginButton = 'input[type=submit]';

    protected $tester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->tester = $I;
    }

    public function login($name, $password)
    {
        $I = $this->tester;

        $I->amOnPage(self::URI);
        $I->fillField(self::usernameField, $name);
        $I->fillField(self::passwordField, $password);
        $I->checkOption(self::rememberMeField);
        $I->click(self::loginButton);
        $I->seeInCurrentUrl('/wordpress/wp-admin/');

        return $this;
    }
}
