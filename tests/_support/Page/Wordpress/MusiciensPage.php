<?php

namespace Page\Wordpress;

/**
 * Class MusiciensPage
 *
 * Enum HTML locator for the musiciens pages in the wordpress plugin.
 */
class MusiciensPage extends WordpressPage
{
    const PATH = 'wp-admin/admin.php?page=QE4_';

    /**
     * URI of single musicien in the wordpress plugin
     * The name of the musicien is required.
     * It's recommanded to use self::getMusicienURI() method
     */
    const URI = self::URL.self::PATH;

    /**
     * The HTML form field encart locator
     */
    const encartField = 'encart';

    /**
     * The HTML form field complement locator
     */
    const complementField = 'complement';

    /**
     * Return the single musicien URI
     */
    static public function getMusicienURI($name)
    {
        return self::URI.$name;
    }
}
