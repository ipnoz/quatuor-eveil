<?php

namespace Page\Wordpress;

/**
 * Class AnnouncePage
 *
 * Enum HTML locator for the announce page in the wordpress plugin.
 */
class AnnouncePage extends WordpressPage
{
    const PATH = 'wp-admin/admin.php?page=QE4_announce';

    /**
     * URI of the announce in the wordpress plugin
     */
    const URI = self::URL.self::PATH;

    /**
     * The HTML checkbox locator for activate the announce
     */
    const activateField = 'isActive';

    /**
     * The HTML input field for the title element
     */
    const titleField = 'title';

    /**
     * The HTML input field for the encart top element
     */
    const encartTopField = 'encart_top';

    /**
     * The HTML input field for the encart bottom element
     */
    const encartBottomField = 'encart_bottom';

    /**
     * The HTML textarea field for the message element
     */
    const messageField = 'message';

    /**
     * The HTML input field for the url1 element
     */
    const url1Field = 'url1';

    /**
     * The HTML input field for the url2 element
     */
    const url2Field = 'url2';
}
