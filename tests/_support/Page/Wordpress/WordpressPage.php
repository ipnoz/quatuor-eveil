<?php

namespace Page\Wordpress;

/**
 * Class WordpressPage
 *
 * Common elements for the wordpress pages
 */
class WordpressPage
{
    /**
     * The wordpress web server locator
     */
    const URL = 'http://localhost/wordpress/';

    /**
     * The HTML form submit button element locator
     */
    const submitButton = 'input[type=submit]';

    /**
     * The wordpress plugin page locator
     */
    const plugins = self::URL.'wp-admin/plugins.php';
}
