<?php

if (!defined('WPINC')) {
    die('You cannot call this script directly');
}

/**
 * Register the css file for the qe4-plugin,
 * for login page, wordpress site web and admin dashboard.
 */
function QE4_add_css_file()
{
    wp_enqueue_style('QE4', QE4_PLUGIN_URI.'/wp-plugin-qe4-styles.min.css');
}

add_action('login_enqueue_scripts', 'QE4_add_css_file');
add_action('wp_enqueue_scripts', 'QE4_add_css_file');
add_action('admin_enqueue_scripts', 'QE4_add_css_file');

/**
 * Create the QE4 table.
 */
function QE4_db_setup()
{
    global $wpdb;
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE ".QE4_db_table_name." (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
		qe4_key tinytext NOT NULL,
		qe4_value text DEFAULT '' NOT NULL,
		PRIMARY KEY  (id)
	) $charset_collate;";

    dbDelta($sql);
}

/**
 * Setup the left menu in the dashboard.
 */
function QE4_dashboard_menu()
{
    // Main index
    add_menu_page(
        'Quatuor Eveil',
        'Quatuor Eveil',
        'manage_options',
        'QE4_index',
        'QE4_index_page_html',
        'dashicons-playlist-audio',
        4
    );
    // Sub menu: Announce
    add_submenu_page(
        'QE4_index',
        'Annonce',
        'Annonce',
        'manage_options',
        'QE4_announce',
        'QE4_announce_page_html'
    );
    // Sub menu: All musiciens
    foreach (QE4_MUSICIENS as $slug => $name) {
        add_submenu_page(
            'QE4_index',
            $name,
            $name,
            'manage_options',
            'QE4_'.$slug,
            'QE4_musiciens_page_html'
        );
    }
}

add_action('admin_menu', 'QE4_dashboard_menu');

/**
 * Disable unused default admin menu in the dashboard.
 */
function QE4_remove_default_dashboard_menu()
{
    remove_menu_page( 'jetpack' );                    // Jetpack*
    remove_menu_page( 'edit.php' );                   // Posts
    remove_menu_page( 'edit.php?post_type=page' );    // Pages
    remove_menu_page( 'edit-comments.php' );          // Comments
    remove_menu_page( 'themes.php' );                 // Appearance
}

add_action('admin_menu', 'QE4_remove_default_dashboard_menu');

/**
 * Disable default dashboard widgets
 */
function QE4_remove_dashboard_widgets()
{
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side'); //Removes the 'Quick Draft' widget
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side'); //Removes the 'Recent Drafts' widget
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal'); //Removes the 'incoming links' widget
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal'); //Removes the 'plugins' widget
    remove_meta_box('dashboard_primary', 'dashboard', 'normal'); //Removes the 'WordPress News' widget
    remove_meta_box('dashboard_secondary', 'dashboard', 'normal'); //Removes the secondary widget
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); //Removes the 'Activity' widget
    remove_meta_box('dashboard_right_now', 'dashboard', 'normal'); //Removes the 'At a Glance' widget
    remove_meta_box('dashboard_activity', 'dashboard', 'normal'); //Removes the 'Activity' widget (since 3.8)
    remove_meta_box('rg_forms_dashboard', 'dashboard', 'normal'); //Removes the 'Activity' widget (since 3.8)
    remove_action('admin_notices', 'update_nag');
}

add_action('admin_init', 'QE4_remove_dashboard_widgets');

/**
 * Remove the screen option and help tabs on admin toolbar
 */
function QE4_remove_help_tabs()
{
    $screen = get_current_screen();
    $screen->remove_help_tabs();
}

add_action('admin_head', 'QE4_remove_help_tabs');
add_filter('screen_options_show_screen', '__return_false');

/**
 * Customize admin toolbar
 */
function QE4_admin_toolbar_render()
{
    global $wp_admin_bar;

    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('view-site');
    $wp_admin_bar->remove_menu('dashboard');
    $wp_admin_bar->remove_menu('themes');
    $wp_admin_bar->remove_menu('widgets');
    $wp_admin_bar->remove_menu('menus');
    $wp_admin_bar->remove_menu('customize');
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('new-content');
}

add_action('wp_before_admin_bar_render', 'QE4_admin_toolbar_render');

/**
 * Add a dashboard link in the admin toolbar in place of the site-name menu.
 * Only if the admin is not in the dashboard.
 */
function QE4_add_dashboard_link_in_toolbar($wp_admin_bar)
{
    global $wp_admin_bar;

    $wp_admin_bar->remove_menu('site-name');

    $args = array(
        'id' => 'qe4_dashboard',
        'title' => 'Tableau de bord',
        'href' => get_dashboard_url(),
    );
    $wp_admin_bar->add_node($args);
}

function QE4_remove_sitename_in_toolbar()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('site-name');
}

if (1 !== preg_match('#/wp-admin/#', $_SERVER['REQUEST_URI'])) {
    add_action('admin_bar_menu', 'QE4_add_dashboard_link_in_toolbar', 1);
    add_action('wp_before_admin_bar_render', 'QE4_remove_sitename_in_toolbar');
}

/**
 * Link the login logo to the login page
 */
function QE4_login_logo_url()
{
    return wp_login_url();
}

add_filter('login_headerurl', 'QE4_login_logo_url');

/**
 * Erase the logo title
 */
function QE4_login_logo_url_title()
{
    return '';
}

add_filter('login_headertext', 'QE4_login_logo_url_title');

