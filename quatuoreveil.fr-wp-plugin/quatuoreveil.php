<?php
/**
 * Plugin Name: Quatuor Eveil WP plugin
 * Description: Wordpress plugin for https://www.quatuoreveil.fr/
 * Author: Dadon David
 * Author URI: https://www.ipnoz.net/
 * Version: 1.0.0
 * License: MIT
 */

if (!defined('WPINC')) {
    die('You cannot call this script directly');
}

error_reporting(-1);

global $wpdb;

/**
 * Define the absolute path name of the plugin
 */
define('QE4_ABSPATH', dirname(__FILE__) . DIRECTORY_SEPARATOR);

/**
 * Define the http URI path name of the plugin
 */
define('QE4_PLUGIN_URI', plugins_url(basename(__DIR__)));

/**
 * Define the database table name of the plugin
 */
define('QE4_db_table_name', $wpdb->prefix.'QE4');

/**
 * Define all musiciens: struc { $slug => $name }
 */
define('QE4_MUSICIENS', [
    'audrey' => 'Audrey',
    'clement' => 'Clément',
    'francois' => 'François',
    'yoann' => 'Yoann'
]);

/**
 * include mandatory files
 */
require_once 'function.php';
require_once 'routes.php';
require_once 'qe4-customize.php';

/**
 * Actions when the admin activate the plugin.
 * Must be in the main file.
 */
function QE4_on_plugin_activation()
{
    QE4_db_setup();
}

register_activation_hook(__FILE__, 'QE4_on_plugin_activation');

/**
 * Query datas (Announce, Musiciens biography) of the plugin
 */
$QE4_datas = $wpdb->get_results('SELECT * FROM '.QE4_db_table_name);

/**
 * Handle (POST) commands
 */
if (isset($_POST['QE4_cmd'])) {

    if ($_POST['QE4_cmd'] === 'QE4_annonce_cmd') {
        QE4_db_update('annonce_isActive', isset($_POST['isActive']));
        QE4_db_update('annonce_title', $_POST['title']);
        QE4_db_update('annonce_encart_top', $_POST['encart_top']);
        QE4_db_update('annonce_encart_bottom', $_POST['encart_bottom']);
        QE4_db_update('annonce_message', $_POST['message']);
        QE4_db_update('annonce_url1', $_POST['url1']);
        QE4_db_update('annonce_url2', $_POST['url2']);
    }

    elseif ($_POST['QE4_cmd'] === 'QE4_biography_cmd') {
        QE4_db_update('bio_encart_'.$_POST['QE4_slug'], $_POST['encart']);
        QE4_db_update('bio_complement_'.$_POST['QE4_slug'], $_POST['complement']);
    }

//    header('Location:'. $_POST['_wp_http_referer']); die;
    header('Location:'.$_SERVER['HTTP_REFERER']); die;
}
