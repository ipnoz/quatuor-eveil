<?php

if (!defined('WPINC')) {
    die('You cannot call this script directly');
}

/**
 * helper for the HTML checkbox
 *
 * return checked="checked" on true
 */
function html_checked($bool)
{
    if (true === $bool) {
        return 'checked="checked"';
    }
}

/**
 * Helper
 *
 * Return the value of a QE4 db key
 */
function QE4_getOption($key)
{
    global $QE4_datas;

    foreach($QE4_datas as $data) {
        if ($data->qe4_key === $key) {
            return $data->qe4_value;
        }
    }
}

/**
 * Helper
 *
 * Update the value of a QE4 db key, if the key doesn't exit, create it.
 */
function QE4_db_update($key, $value)
{
    global $wpdb, $QE4_datas;

    foreach($QE4_datas as $data) {
        if ($data->qe4_key === $key) {
            $wpdb->update(
                QE4_db_table_name,
                ['qe4_key' => $key, 'qe4_value' => $value],
                ['id' => $data->id]
            );
            $updated = true;
            break;
        }
    }

    if (!isset($updated)) {
        $wpdb->insert(
            QE4_db_table_name,
            ['qe4_key' => $key, 'qe4_value' => $value]
        );
    }
}
