<?php

if (!defined('WPINC')) {
    die('You cannot call this script directly');
}

$isActive = html_checked((bool)QE4_getOption('annonce_isActive'));
$title = QE4_getOption('annonce_title');
$encartTop = QE4_getOption('annonce_encart_top');
$encartBottom = QE4_getOption('annonce_encart_bottom');
$message = QE4_getOption('annonce_message');
$url1 = QE4_getOption('annonce_url1');
$url2 = QE4_getOption('annonce_url2');

?>
<div class="wrap">

    <h1>Quatuor Eveil: Annonce</h1>

    <form method="post">

        <input type="hidden" name="QE4_cmd" value="QE4_annonce_cmd">

        <table class="form-table">

            <tr>
                <th></th>
                <td>
                    <input type="checkbox" id="isActive" name="isActive" <?= $isActive; ?>/>
                    <label for="isActive"><strong>Afficher l'annonce</strong></label>
                </td>
            </tr>

            <tr>
                <th>
                    Titre
                </th>
                <td>
                    <input type="text" name="title" class="regular-text" value="<?= $title; ?>"/>
                </td>
            </tr>

            <tr>
                <th>
                    Encart (ligne du haut)
                </th>
                <td>
                    <input type="text" name="encart_top" class="regular-text" value="<?= $encartTop; ?>"/>
                </td>
            </tr>

            <tr>
                <th>
                    Encart (ligne du bas)
                </th>
                <td>
                    <input type="text" name="encart_bottom" class="regular-text" value="<?= $encartBottom; ?>"/>
                </td>
            </tr>

            <tr>
                <th>
                    Message
                </th>
                <td>
                    <textarea name="message" cols="50" rows="10" class="large-text"><?= $message; ?></textarea>
                </td>
            </tr>

            <tr>
                <th>
                    URL (image)
                </th>
                <td>
                    <input type="url" name="url1" class="large-text" value="<?= $url1; ?>"/>
                </td>
            </tr>

            <tr>
                <th>
                    URL2 (vidéo)
                </th>
                <td>
                    <input type="url" name="url2" class="large-text" value="<?= $url2; ?>"/>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <input type="submit" name="submit" id="submit" class="button button-primary" value="Sauvegarder"/>
                </td>
            </tr>

        </table>
    </form>
</div>
