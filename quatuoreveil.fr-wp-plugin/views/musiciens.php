<?php

if (!defined('WPINC')) {
    die('You cannot call this script directly');
}

$slug = str_replace('QE4_', '', $_GET['page']);
$name = QE4_MUSICIENS[$slug];

$encart = QE4_getOption('bio_encart_'.$slug);
$complement = QE4_getOption('bio_complement_'.$slug);

?>
<div class="wrap">

    <h1>Quatuor Eveil: <?= $name; ?></h1>

    <form method="post">

        <input type="hidden" name="QE4_cmd" value="QE4_biography_cmd">
        <input type="hidden" name="QE4_slug" value="<?= $slug; ?>">

        <table class="form-table">

            <tr>
                <th>
                    Encart
                </th>
                <td>
                    <textarea name="encart" cols="50" rows="10" class="large-text"><?= $encart; ?></textarea>
                </td>
            </tr>

            <tr>
                <th>
                    Complément
                </th>
                <td>
                    <textarea name="complement" cols="50" rows="10" class="large-text"><?= $complement; ?></textarea>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <input type="submit" name="submit" id="submit" class="button button-primary" value="Sauvegarder"/>
                </td>
            </tr>

        </table>
    </form>
</div>
