<?php

/**
 * Routes for QE4 admin pages
 */
function QE4_index_page_html()
{
    if (current_user_can('manage_options')) {
        require_once QE4_ABSPATH . 'views/index.php';
    }
}

function QE4_announce_page_html()
{
    if (current_user_can('manage_options')) {
        require_once QE4_ABSPATH . 'views/announce.php';
    }
}

function QE4_musiciens_page_html()
{
    if (current_user_can('manage_options')) {
        require_once QE4_ABSPATH . 'views/musiciens.php';
    }
}
